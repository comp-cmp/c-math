#include <stdio.h>
#include <math.h>
#define E M_E
#define PI M_PI
// Rectangle exhastion
double method_of_exhaustion(int N, double a, double b, double a1, double (*function)(double, double)) {
    double sum = 0;
    for(int n = 1; n <= pow(2, N - 1); n++) sum += function(((2*n - 1) * b + (pow(2, N) - (2*n - 1)) * a) / pow(2, N), a1);
    return (b - a) * sum / (pow(2, N-1));
}

double testing(double x, double lambda) { return pow(E, lambda*pow(cos(x), 2)); }

int main(char** args, int argc) {
	printf("%f\n", method_of_exhaustion(20, 0, 10*PI, 1, testing));
}

