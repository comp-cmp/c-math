SHELL:=/bin/bash
CMAKE_CPPFLAGS += -Wno-c++11-extensions
GCC_FLAGS:= -lm
CC:=gcc
TO_COMPILE:=$(shell ls *.c)

all : $(TO_COMPILE)

moe: method_of_exhaustion.c
	$(CC) -o moe method_of_exhaustion.c $(GCC_FLAGS)

sommerfeld: sommerfeld.cpp
	g++ $(CMAKE_CPPFLAGS) -o sommerfeld sommerfeld.cpp
clean:
	rm -rf sommerfeld
